= Command Line Interface
:keywords: CLI, testing-farm

Testing Farm provides a CLI tool for easy interaction with Testing Farm.

== Use cases

Currently, the CLI targets these main use cases:

* submitting a test request

* watch a request for completion

* get a request completion report

* easy integration with CI systems via small container image `quay.io/testing-farm/cli`

* reservation of a test environment

== Installation

See the https://gitlab.com/testing-farm/cli/-/blob/main/README.adoc#user-content-installation[CLI installation documentation].

== First steps

For the CLI to work, make sure to export the `TESTING_FARM_API_TOKEN` in your environment.
If you do not yet have a token, see xref:onboarding.adoc[onboarding].

Testing of the project is currently done via:

[source,shell]
....
export TESTING_FARM_API_TOKEN=<your-token>
....

[NOTE]
====
The token is specific only to a single ranch.
If you want to use more ranches, you will need a token for each of them.
====

Testing Farm provides a sample tmt plan to try out the CLI tool.
This plan runs a single test which by default prints out `cat /etc/os-release`.

* 🌍 Public Ranch - run sanity test against Fedora Rawhide
+
[source,shell]
....
testing-farm request --compose Fedora-Rawhide --git-url https://gitlab.com/testing-farm/tests --plan /testing-farm/sanity
....

+
* 🎩 Red Hat Ranch
+
[source,shell]
....
testing-farm request --compose RHEL-9.2.0-Nightly --git-url https://gitlab.com/testing-farm/tests --plan /testing-farm/sanity
....

If your test is expected to take a long time, you should set the `--timeout` parameter so that your test is not terminated prematurely.
The default is currently 12 hours.
Try your `tmt` test locally first to get an idea of how long it will take - perhaps using `time`.
The timeout is specified in minutes. For example:

[source,shell]
....
testing-farm request --compose RHEL-9.2.0-Nightly --git-url https://gitlab.com/myproject --plan /tests/bigtest --timeout 360
....

[#usage]
== Usage

[#reserve]
=== Test Environment Reservation

The `testing-farm reserve` command allows you to reserve a system in the Testing Farm environment for testing purposes.
This is particularly useful for debugging unexpected test failures.

[WARNING]
====
The reservation in public requires the user to share with us his public IP.
Refer to the xref:contact.adoc[Contacts] section to contact us.
====

To reserve a system, use the following command:

[source,shell]
....
testing-farm reserve --compose Fedora-Rawhide
....

By default, the CLI sets the reservation duration to 30 minutes.
You can change this duration using the `--duration MINUTES` option.
For example:

[source,shell]
....
testing-farm reserve --compose Fedora-Rawhide --duration 60
....

Or by using the `extend-reservation MINUTES` command within the machine:
[source,shell]
....
extend-reservation 60
....

[NOTE]
====
The maximum reservation time is 12 hours (720 minutes)
====

SSH is used for accessing the reserved machine.
The CLI will, by default, copy all public keys that match the pattern `~/.ssh/*.pub` to the machine.
You can specify a different public key using the `--ssh-public-key PATH` option.
Globbing can be used to specify multiple keys.
For instance:

[source,shell]
....
testing-farm reserve --compose Fedora-Rawhide --ssh-public-key /path/to/my/keys/*.pub
....

[NOTE]
====
For the container based installation, the option is unsupported.
====

Similar to the `testing-farm request` command, the `reserve` command provides various options to customize the testing environment.
You can view all available options using the `--help` option:

[source,shell]
....
testing-farm reserve --help
....

See xref:test-environment.adoc[Test Environment] and xref:test-request.adoc#hardware[Hardware Requirements] pages to find more information.

If you need to return the machine early, execute the following command within the machine:

[source,shell]
....
return2testingfarm
....
